"""
Contains simple functions to prepare Maize genome release 5B for
processing by faToTwoBit. There is no main method - functions are
meant to be run interactively from inside the python interpreter.
This is more or less a one-off.
"""

import Utils.General as utils

# grep '>' to get this
heads = """>chromosome:AGPv2:1:1:301354135:1 chromosome 1
>chromosome:AGPv2:2:1:237068873:1 chromosome 2
>chromosome:AGPv2:3:1:232140174:1 chromosome 3
>chromosome:AGPv2:4:1:241473504:1 chromosome 4
>chromosome:AGPv2:5:1:217872852:1 chromosome 5
>chromosome:AGPv2:6:1:169174353:1 chromosome 6
>chromosome:AGPv2:7:1:176764762:1 chromosome 7
>chromosome:AGPv2:8:1:175793759:1 chromosome 8
>chromosome:AGPv2:9:1:156750706:1 chromosome 9
>chromosome:AGPv2:10:1:150189435:1 chromosome 10
>chromosome:AGPv2:mitochondrion:1:569630:1 chromosome mitochondrion
>chromosome:AGPv2:chloroplast:1:140384:1 chromosome chloroplast
>chromosome:AGPv2:UNKNOWN:1:7140151:1 chromosome UNKNOWN"""

def getHeads():
    "Get mapping of fasta headers to chromosomes"
    d = {}
    vals = heads.split('\n')
    for val in vals:
        toks = val.split()
        chrom = toks[-1]
        if chrom == 'mitochondrion':
            chrom = 'Mt'
        elif chrom == 'chloroplast':
            chrom = 'Pt'
        d[val]=chrom
    return d

def fixFasta(fn='Z_mays_B73_Mar_2010.fa'):
    "Make a new fasta file with headers we want; faToTwoBit will use these to name the chromosomes, and
    the chromosome names need to match what are used in the annotations files."
    fh = utils.readfile(fn)
    outfh = open('Z_mays_B73_Mar_2010_fixed.fa','w')
    d = getHeads()
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            outfh.close()
            break
        if line.startswith('>'):
            key = line.rstrip()
            newline = d[key]
            outfh.write(">%s\n"%newline)
        else:
            outfh.write(line)
    fh.close()

