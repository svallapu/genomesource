#!/usr/bin/env python

"""
Build new link.psl files in which probes and consensus alignments are merged
into the same row to allow indexing by tabix.

Accepts input on stdin and outputs converted data on stdout.
Run this as:

$ gunzip -c file.link.psl.gz | convertLinkPsl.py > output.link.psl

or

$ gunzip -c file.link.psl.gz | convertLinkPsl.py | sort -k14,14 -k16,16n | bgzip > newfile.link.psl.gz
$ tabix -s 14 -b 16 -e 17 newfile.link.psl.gz

The link.psl format developed by Affymetrix divides alignments into two sections.

The first section contains consensus sequences aligned onto the genome. A consensus sequence
may align multiple times.

A later section contains probe alignments onto the consensus sequence. Typically, a probe has only
one location on the consensus, but it may have more than one. Also, a probe may end being split
across an intron once the consensus sequences is projected (aligned) onto the reference genomic
sequence.
"""

import os,sys,optparse,fileinput

def main():
    (first,second)=readLinkPsl()
    fh = sys.stdout
    writeNewLinkPsl(first,second,fh)

def readLinkPsl():
    cons = {} # consensus sequences, what was aligned onto the genome 
    probes = {} # alignments of probes on the consensus seuqences
    onconsensus=False # whether we're parsing the consensus part of the file
    onprobesets=False # whether we're parsing the probes part of the file
    first={} # consensus sequence section, keys are ids
    second={} # probe section, keys are probe set ids
    for line in fileinput.input():
        if line.startswith('#'):
            continue
        if line.startswith('track') and 'consensus' in line:
            onconsensus=True
            continue
        elif line.startswith('track') and 'probesets' in line:
            onconsensus=False
            onprobesets=True
            continue
        elif line.startswith('track'):
            break
        toks = line.rstrip().split('\t')
        if len(toks)<=1:
            continue
        if onconsensus:
            name=toks[9]
            if first.has_key(name):
                first[name].append(toks) # a consensus may align multiple times
            else:
                first[name]=[toks]
        elif onprobesets:
            name=toks[13]
            second[name]=toks
    return (first,second)

def writeNewLinkPsl(first,second,fh):
    for name in first.keys():
        tokss = first[name]
        probetoks = second[name]
        for toks in tokss:
            newtoks = toks+probetoks
            line='\t'.join(newtoks)+'\n'
            fh.write(line)

if __name__ == '__main__':
    main()
    
