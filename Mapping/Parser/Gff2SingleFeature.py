"""Functions for reading GFF2 files containing features like repeats that have no children, i.e.,
are not CompoundDNASeqFeatures. Does not try to read the extra feature field. Saves it at as
single key/value pair. Key is string "extra_feat".
"""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys
import warnings

def gff2feats(fname=None,fh=None):
    """
    Function: read features from a GFF format file
    Returns : list of DNASeqFeature objects representing single-span 
              features
    Args    : fname - name of file to read
    """
    feats=[]
    lineNumber = 0
    if not fh and fname:
        fh = utils.readfile(fname)
    else:
        raise ValueError("gff2feats requires name of file or file stream.")
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        if len(line.rstrip())==0: # ignore blank lines
            continue
        try:
            (seqname,producer,feat_type,start,end,
             score,strand,frame,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        key_vals = {'extra_feat':extra_feat}
        start = int(start)
        end = int(end)
        length = end - start
        if strand == '+':
            strand = 1
        elif strand == '-':
            strand = -1
        elif strand == '.':
            strand = 0 # strand is not relevant or we don't know what it is
        else:
            raise ValueError("Strand makes no sense for line %i: %s"%(lineNumber,line))
        try:
            feat = feature.DNASeqFeature(seqname=seqname,
                                        start=start,
                                        producer=producer,
                                        score=score,
                                        length=length,
                                        strand=strand,
                                        key_vals=key_vals,
                                        feat_type=feat_type)
            feats.append(feat)
        except ValueError:
            sys.stderr.write("Error on: %s\n"%line)
            raise
    return feats

            
        
