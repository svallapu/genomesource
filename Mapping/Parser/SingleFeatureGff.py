"""Functions for reading GFF3 files."""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys
import warnings

"""
This contains functions for parsing GFF3 in which the producer field is preserved as
a key/value attribute.
"""

def gff2feats(fname=None,fh=None):
    """
    Function: read features from a GFF3 format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    if not fh and fname:
        fh = utils.readfile(fname)
    else:
        raise ValueError("gff2feats requires name of file or file stream.")
    mRNAs = {}
    sub_feats={}
    genes={}
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        if len(line.rstrip())==0: # ignore blank lines
            continue
        try:
            (seqname,producer,feat_type,start,end,
             ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        if feat_type == 'gene':
            key_vals=parseKeyVals(extra_feat)
            gene_id=key_vals['ID']
            if genes.has_key(gene_id):
                warnings.warn("Two lines with the same gene id: %s"%gene_id)
                #raise ValueError("Two lines with the same gene id: %s"%gene_id)
            genes[gene_id]=key_vals 
        if feat_type in ['mRNA','transcript','CDS','exon']:
            start = int(start)
            end = int(end)
            if end < start and strand=='-':
                sys.stderr.write("Warning: end less than start for - strand feature at line %i. Reversing start and end.\n%s" % (lineNumber,line))
                tmp = end
                start = tmp
                end = start
            
            start = start-1
            if start == -1:
                sys.stderr.write("Warning: GFF-to-BED conversion created -1 start at line %i. Adding 1 to start.\n%s" % (lineNumber,line))
                start=start+1
            end = end
            length=end-start
            if strand == '+':
                strand = 1
            elif strand == '-':
                strand = -1
            elif strand == '.':
                strand = 0 # strand is not relevant or we don't know what it is
            else:
                raise ValueError("Strand makes no sense for line %i: %s"%(lineNumber,line))
            key_vals = parseKeyVals(extra_feat)
            key_vals['producer']=producer
            if feat_type=='mRNA' or feat_type=='transcript': # Augustus emits transcript, not mRNA
                display_id=key_vals['ID']
                try:
                    feat = feature.CompoundDNASeqFeature(seqname=seqname,
                                                        display_id=display_id,
                                                        start=start,
                                                        length=length,
                                                        strand=strand,
                                                        key_vals=key_vals,
                                                        feat_type='mRNA')
                except ValueError:
                    sys.stderr.write("Error on: %s\n"%line)
                    raise
                if mRNAs.has_key(display_id):
                    raise ValueError("Two mRNAs have the same ID: %s"%ID)
                mRNAs[display_id]=feat
            elif feat_type in ['exon','CDS']:
                if feat_type=='CDS' and strand==-1 and start==0:
                    start=1 # an evil hack, due to TAU sucking
                try:
                    feat = feature.DNASeqFeature(seqname=seqname,
                                                 start=start,
                                                 length=length,
                                                 strand=strand,
                                                 key_vals=key_vals,
                                                 feat_type=feat_type)
                except ValueError:
                    sys.stderr.write("Error on: %s\n"%line)
                    raise 
                try:
                    parent_ID=key_vals['Parent']
                except KeyError:
                    raise KeyError("No Parent for for line %i"%lineNumber)
                # some GFF3 files assign the same feature to multiple parents in a misguided
                # attempt to make this verbose format more compact
                parents=parent_ID.split(',')
                for parent in parents:
                    if not sub_feats.has_key(parent):
                        sub_feats[parent]=[]
                    sub_feats[parent].append(feat)
            else:
                raise ValueError("Unrecognized feat type for line %i"%lineNumber)
            key_vals = parseKeyVals(extra_feat)
    for parent_ID in sub_feats.keys():
        try:
            mRNA = mRNAs[parent_ID]
        except KeyError:
            sys.stderr.write("Warning: no mRNA for feature with parent id: %s.\n"%parent_ID)
            continue
        for sub_feat in sub_feats[parent_ID]:
            mRNA.addFeat(sub_feat)
    return mRNAs.values()

def parseKeyVals(extra_feat):
    "Parse extra feature fields."
    if extra_feat.endswith(';'):
        vals = extra_feat.split(';')[:-1]
    else:
        vals = extra_feat.split(';')
    key_vals = {}
    # because this:
    # ID=Csa01g001070.1;Name=Csa01g001070.1;Parent=Csa01g001070;Note=transferases;sulfuric ester hydrolases;catalytics;transferases
    prev_key=None
    for item in vals:
        pair=item.split('=')
        if not len(pair)==2:
            warnings.warn("GFF line has weird extra feature value: %s."%extra_feat)
            if len(pair)==1:
                key=prev_key
                val=key_vals[key]+';'+pair[0]
        else:
            key = pair[0]
            val = pair[1]
            if key_vals.has_key(key):
                raise ValueError("GFF line has two values for same key in extra feat field: %s."%extra_feat)
            prev_key=key
        key_vals[key]=val
    return key_vals
            
        
