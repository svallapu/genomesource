#!/usr/bin/env python

import unittest
import os,sys

import Mapping.FeatureModel as M
import Mapping.Parser.Bed as b

class Bed2Feats(unittest.TestCase):

   test_100_lines = 'data/Bed/100-lines.bed'

   def testReadBed(self):
      "bed2feats should create 100 CompoundDNASeqFeature objects from the test data"
      feats = b.bed2feats(fname=self.test_100_lines,
                           producer='TEST',
                           has_orf=1)
      self.assertEquals(len(feats),100)
                           
if __name__ == "__main__":
    unittest.main()
    
