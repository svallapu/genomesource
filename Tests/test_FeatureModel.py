#!/usr/bin/env python

import unittest
import os,sys

import Mapping.FeatureModel as M

class KeyValTest(unittest.TestCase):

   def testEmptyKeyVals(self):
      "If we make a new KeyVals but don't give it any data, getKeyVals should return None"
      kv = M.KeyVal()
      self.assertEquals(kv.getKeyVals(),None)

   def testSetKeyVals(self):
      "If we make a new KeyVals and give a dictionary, getKeyVals should return the same dictionary."
      knownDict = {"First":1}
      kv = M.KeyVal()
      kv.setKeyVals(knownDict)
      self.assertEquals(kv.getKeyVals(),knownDict)

   def testSetKeyVal(self):
      "If we set a value using a key, we should get back that same value using the same key."
      kv = M.KeyVal()
      kv.setKeyVal("Freedom","Fries")
      self.assertEquals(kv.getVal("Freedom"),"Fries")

   def testUnhashableKey(self):
      "If we try to set a value with an unhashable key, a KeyVal should raise a TypeError"
      kv = M.KeyVal()
      self.assertRaises(TypeError,kv.setKeyVal,{},'foo')

   def testNonExistantKey(self):
      "getVal should return None if we ask for a key that isn't there."
      kv = M.KeyVal()
      self.assertEquals(kv.getVal('foo'),None)
      
   def testGetVal(self):
      "getVal should return the same value for a previously set key"
      kv = M.KeyVal()
      val = {}
      kv.setKeyVal('foo',val)
      self.assertEquals(kv.getVal('foo'),val)

class RangeTest(unittest.TestCase):
   
   """Test Data
  s1   s1+len1 s2  s2+len2
   |     |     |     |
    nnnnn nnnnn nnnnn nnnnn nnnnn
       |                   |
      s3                 s3+len3 |
                          s4    s4+len4
   """
   
   """_seqs[0] = (start,length)"""
   _seqs = ((0,5),
            (10,5),
            (3,17),
            (20,5))

   def setUp(self):
      self.object = M.Range(self._seqs[0][0],self._seqs[0][1])
      self.s1 = M.Range(self._seqs[0][0],self._seqs[0][1])
      self.s2 = M.Range(self._seqs[1][0],self._seqs[1][1])         
      self.s3 = M.Range(self._seqs[2][0],self._seqs[2][1])
      self.s4 = M.Range(self._seqs[3][0],self._seqs[3][1])
      var = "Hello World"
      
   def tearDown(self):
      self.object = None
      self.s1 = None
      self.s2 = None
      self.s3 = None
      self.s4 = None

   def testGetStart(self):
      "If we set start in the constructor, getStart should get that same value."
      self.assertEquals(self.object.getStart(),self._seqs[0][0])

   def testDefaultStartIsNone(self):
      "If we create a new Range without giving it any arguments, start should be None"
      r = M.Range()
      self.assertEquals(r.getStart(),None)

   def testDefaultStartIsNone(self):
      "If we create a new Range without giving it any arguments, length should be None"
      r = M.Range()
      self.assertEquals(r.getLength(),None)

   def testSetStart(self):
      "If we make a new Range and then set the start, then getStart should return that same value."
      r = M.Range(start=1,length=10)
      self.object.setStart(3)
      self.assertEquals(self.object.getStart(),3)
   
   def testNegIntBadInputSetStart(self):
      "setStart should raise ValueError when given negative coordinates."
      r = M.Range()
      self.assertRaises(ValueError,r.setStart,-1)

   def testWrongTypeInputSetStart(self):
      "setStart shoudl object when given non-int input."
      r = M.Range()
      self.assertRaises(TypeError,r.setStart,'-1')
   
   def testGetLength(self):
      """Tests if get length returns correct value"""
      self.assertEquals(self.object.getLength(),self._seqs[0][1])

   def testSetLength(self):
      """Tests if set length works"""
      self.object.setLength(3)
      self.assertEquals(self.object.getLength(),3)
   
   def testBadInputSetLength(self):
      """Tests if set start throws exception with bad input"""
      self.assertRaises(ValueError, self.object.setLength,-1)
      
   def testOverlaps(self):
      """Tests if overlaps correctly calculates overlaps"""
      self.assertEquals(self.s1.overlaps(self.s2),False)
      self.assertEquals(self.s1.overlaps(self.s3),True)
      self.assertEquals(self.s2.overlaps(self.s3),True)
      self.assertEquals(self.s2.overlaps(self.s1),False)
      self.assertEquals(self.s3.overlaps(self.s1),True)
      self.assertEquals(self.s3.overlaps(self.s2),True)
      self.assertEquals(self.s3.overlaps(self.s4),False)
      self.assertEquals(self.s4.overlaps(self.s3),False)
            
   def testContains(self):
      """Tests if contains works"""
      self.assertEquals(self.s1.contains(self.s2),False)
      self.assertEquals(self.s2.contains(self.s1),False)
      self.assertEquals(self.s1.contains(self.s3),False)
      self.assertEquals(self.s3.contains(self.s1),False)
      self.assertEquals(self.s2.contains(self.s3),False)
      self.assertEquals(self.s3.contains(self.s2),True)

   def testEquals(self):
      """Tests if equals works"""
      s1 = M.Range(self._seqs[0][0],self._seqs[0][1])
      s2 = M.Range(self._seqs[1][0],self._seqs[1][1])

      self.assertEquals(self.s1.equals(self.s2),False)
      self.assertEquals(self.s1.equals(self.object),True)


class SeqFeatureTest(unittest.TestCase):

   def testDefaultsAreNone(self):
      "If we make a new SeqFeature with no arguments, initial values should be None"
      s = M.SeqFeature()
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getSeqname(),None)
      self.assertEquals(s.getFeatType(),None)
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getStart(),None)

   def testSetToNone(self):
      "If we create a SeqFeature, set its display id, and then reset to None, the new value should be None"
      s = M.SeqFeature()
      s.setDisplayId('foo')
      s.setDisplayId(None)
      self.assertEquals(s.getDisplayId(),None)


class DNASeqFeatureTest(unittest.TestCase):

   def testWrongStrandValue(self):
      "setStrand should object if given a value not in [1,-1,None]"
      s = M.DNASeqFeature()
      self.assertRaises(ValueError,s.setStrand,'foo')

   def testContainsNoStrand(self):
      "If DNASeqFeature contains another and neither has a strand, contains should return True"
      outer = M.DNASeqFeature(start=0,length=100)
      inner = M.DNASeqFeature(start=0,length=50)
      self.assertEquals(outer.contains(inner),True)

   def testContainsDiffStrand(self):
      "If DNASeqFeature contains another but they have different strands, contains should return False"
      outer = M.DNASeqFeature(start=0,length=100,strand=1)
      inner = M.DNASeqFeature(start=0,length=50,strand=-1)
      self.assertEquals(outer.contains(inner),False)

   def testEqualsSameStartLengthStrand(self):
      "If DNASeqFeature has same coordinates as another and the same strand, equals should return True"
      this = M.DNASeqFeature(start=0,length=100,strand=1)
      that = M.DNASeqFeature(start=0,length=100,strand=1)
      self.assertEquals(this.equals(that),True)

   def testEqualsDiffStrand(self):
      "If DNASeqFeature has same coordinates as another and different strand, equals should return False"
      this = M.DNASeqFeature(start=0,length=100,strand=-1)
      that = M.DNASeqFeature(start=0,length=100,strand=1)
      self.assertEquals(this.equals(that),False)

class CompoundDNASeqFeatureTest(unittest.TestCase):

   feat_type = 'transcript'
   seqname='X'
   start = 0
   length = 9000
   display_id = 'A'
   strand = 1
   producer='tester'
   strand = 1

   def setUp(self):
      a = M.CompoundDNASeqFeature(start=self.start,
                                  length=self.length,
                                  feat_type=self.feat_type,
                                  seqname=self.seqname,
                                  producer=self.producer,
                                  strand=self.strand,
                                  display_id=self.display_id)
      self.feat = a

   def testCreateCompoundDNASeqFeature(self):
      a = self.feat
      self.assertEquals(a.getLength(),self.length)
      self.assertEquals(a.getStart(),self.start)
      self.assertEquals(a.getFeatType(),self.feat_type)
      self.assertEquals(a.getStrand(),self.strand)
      self.assertEquals(a.getProducer(),self.producer)
      self.assertEquals(a.getDisplayId(),self.display_id)

   def testDefaultsAreNone(self):
      "If we make a new CompoundDNASeqFeature with no arguments, initial values should be None"
      s = M.CompoundDNASeqFeature()
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getSeqname(),None)
      self.assertEquals(s.getFeatType(),None)
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getStart(),None)

   def testSortedVariableChanges(self):
      "When we call getSortedFeats for the first time, _sorted variable switches from False to True."
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,strand=1) # third
      subfeat1 = M.DNASeqFeature(start=0,length=100,strand=1) # first
      subfeat2 = M.DNASeqFeature(start=200,length=100,strand=1) # second
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      self.assertEquals(s._sorted,False)
      feats=s.getSortedFeats()
      self.assertEquals(s._sorted,True)

   def testGetSortedFeats(self):
      "getSortedFeats should return subfeatures sorted by start."
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,strand=1) # third
      subfeat1 = M.DNASeqFeature(start=0,length=100,strand=1) # first
      subfeat2 = M.DNASeqFeature(start=200,length=100,strand=1) # second
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      feats=s.getSortedFeats()
      self.assertEquals(feats[0],subfeat1)
      self.assertEquals(feats[1],subfeat2)
      self.assertEquals(feats[2],subfeat0)

   def testUseSubFeatCoords(self):
      "If we make a new CompoundDNASeqFeature, add a subfeature to it, and call useSubFeatCoords, the CompoundDNASeqFeature should acquire the coordinates and strand of the subfeature."
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,strand=1)
      subfeat1 = M.DNASeqFeature(start=0,length=100,strand=1)
      subfeat2 = M.DNASeqFeature(start=200,length=100,strand=1)
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      s.useSubFeatCoords()
      self.assertEquals(s.getStart(),subfeat1.getStart())
      self.assertEquals(s.getStrand(),subfeat1.getStrand())
      self.assertEquals(s.getEnd(),subfeat0.getEnd())

    
if __name__ == "__main__":
    unittest.main()
    
