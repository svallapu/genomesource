#!/usr/bin/env python

import unittest
import os,sys

import Mapping.Parser.Gff3 as G

class Gff3(unittest.TestCase):

    def testGoodData(self):
      "parseKeyVals should parse all keys in an extra feature field"
      extra_feat='ID=Csa01g001070.1;Name=Csa01g001070.1;Parent=Csa01g001070;Note=Nothing to worry about here'
      key_values = G.parseKeyVals(extra_feat)
      right_answer = 4
      the_answer = len(key_values.keys())
      self.assertEquals(the_answer,right_answer)
      
    def testQuotedDataWithSemicolonCharacters(self):
      "parseKeyVals should handle quoted data containing ; characters"
      extra_feat='ID=Csa01g001070.1;Name=Csa01g001070.1;Parent=Csa01g001070;Note="transferases;sulfuric ester hydrolases;catalytics;transferases"'
      key_values = G.parseKeyVals(extra_feat)
      right_answer = "transferases;sulfuric ester hydrolases;catalytics;transferases"
      the_answer = ""
      if key_values.has_key("Note"):
          the_answer = key_values["Note"]
      self.assertEquals(the_answer,right_answer)
         
    def testQuotedDataWithEqualsCharacters(self):
      "parseKeyVals should handled quoted data containing = characters"
      extra_feat='ID=Csa01g001070.1;Name=Csa01g001070.1;Parent=Csa01g001070;Note="transferases==crazy data==even more crazy data"'
      key_values = G.parseKeyVals(extra_feat)
      right_answer = "transferases==crazy data==even more crazy data"
      the_answer = ""
      if key_values.has_key("Note"):
          the_answer = key_values["Note"]
      self.assertEquals(the_answer,right_answer)

    def testTrailingSemiColo(self):
        "parseKeyVals should handle extra feature fields with semicolon at the end of the line"
        extra_feat='foo="bar and baz";bop="hello world";'
        key_values = G.parseKeyVals(extra_feat)
        right_answer = "hello world"
        the_answer = ""
        if key_values.has_key("bop"):
          the_answer = key_values["bop"]
        self.assertEquals(the_answer,right_answer)

    def testConsecutiveSemiColons(self):
        "parseKeyVals should handle extra feature fields with consecutive semicolon characters. Found in: apple genome gene_models_20170612.gff3.bz2"
        extra_feat="D=ncRNA:MD08G1213800;Name=MD08G1213800;Parent=gene:MD08G1213800;;Note=rRNA:8s rRNA"
        key_values = G.parseKeyVals(extra_feat)
        right_answer = "rRNA:8s rRNA"
        the_answer = ""
        if key_values.has_key("Note"):
          the_answer = key_values["Note"]
        self.assertEquals(the_answer,right_answer)
          
if __name__ == "__main__":
    unittest.main()
    
