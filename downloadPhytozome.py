#!/usr/bin/env python

import os,sys,re,optparse


gv = re.compile(r'[Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec]_[12][90]\d\d$')

def getSpecies(d='.'):
    "Get mapping between species and genome version directories and Phytozome designations."
    dirs = os.listdir(d)
    dct = {}
    for d in dirs:
        if gv.search(d):
            toks = d.split('_')
            phytoname = toks[0]+toks[1]
            dct[d]={'phytoname':phytoname}
    return dct

def downloadSpecies(d=None,phytoname=None,phytoversion=None):
    "Download assemblies and annotations for species hosted at Phytozome"
    base_url = 'ftp://ftp.jgi-psf.org/pub/JGI_data/phytozome/v'+phytoversion+'/'+phytoname
    orig_dir = os.getcwd()
    try:
        os.chdir(d)
        cmd = 'wget --retr-symlinks -nd -nH -q -o %s.%s.assembly.log %s/assembly/*.fa.gz'%(phytoname,phytoversion,base_url) 
        sys.stderr.write("Executing: %s\n"%cmd)
        os.popen(cmd)
        cmd = 'wget --retr-symlinks -nd -nH -q -o %s.%s.annotation.log %s/annotation/*_gene.gff3.gz'%(phytoname,phytoversion,base_url) 
        sys.stderr.write("Executing: %s\n"%cmd)
        os.popen(cmd)
    except Exception:
        sys.stderr.write("Error running commands for %s\n"%phytoname)
    os.chdir(orig_dir)

def main(options,args):
    "Do it all"
    if options.quickload_dir:
        d = options.quickload_dir
    else:
        d = '.'
    dct = getSpecies(d=d)
    phytoversion=options.phytoversion
    for key in dct.keys():
        d = key
        phytoname=dct[key]['phytoname']
        downloadSpecies(d=d,phytoname=phytoname,phytoversion=phytoversion)
        sys.stderr.write("Did: %s\n"%phytoname)
    sys.stderr.write("Fin.\n")

if __name__ == '__main__':
    usage = "%prog [options]\n\nRun this in the same directory as the genome data directories\nyou want to populate with data downloaded from Phytozome."
    parser = optparse.OptionParser(usage)
    parser.add_option("-v","--version",help="Phytozome release version, e.g., 7.0 [required]",
                     dest="phytoversion",default=None,type="string")
    parser.add_option("-q","--quickload_dir",help="Use this directory to set up empty directories for the downloads.",
                      dest="quickload_dir",type="string")
    (options,args)=parser.parse_args()
    if not options.phytoversion:
        parser.error("Supply Phytozome version, e.g., 7.0. See Phytozome.net for details on version.")
    main(options,args)
