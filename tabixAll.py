#!/usr/bin/env python
"Sort and index all the files in a directory, using tabix. Tabix and bgzip should be in the user's PATH."

import os,sys,optparse
from Utils.General import fileExists, runCommand

def getFiles(dir='.'):
    files = os.listdir(dir)
    return files

def processFile(fn=None,files=None,force=False,verbose=False):
    "Name of a file to process."
    if not fn:
        sys.stderr.write("No file to process.\n")
    if not files or len(files)==0:
        sys.stderr.write("No files to process.\n")
    newfiles = map(lambda x:x.split(os.sep)[-1],files)
    files = newfiles
    # figure out what kind of file it is an what needs to be done for it
    f = fn.split(os.sep)[-1]
    toks = f.split('.')
    base = '.'.join(toks[0:-1])
    extension = toks[-1]
    if verbose:
        sys.stderr.write("Processing file with extension: %s\n"%extension)
    if extension in ['psl','bed','bedgraph','wig']:
        if extension == 'psl':
            # see: http://genome.ucsc.edu/FAQ/FAQformat.html#format2
            chrom_field=14
            start_field=16
            end_field=start_field+1
            if verbose:
                sys.stderr.write("Processing a PSL file.\n")
        elif extension in ['bed','bedgraph','wig']:
            chrom_field=1
            start_field=2
            end_field=start_field+1
        if extension == 'wig':
            extension = 'bedgraph'
        first_target = '.'.join([base,extension,'gz'])
        if force or not fileExists(first_target):
            make_compressed(fn,first_target,chrom_field=chrom_field,
                            start_field=start_field,
                            verbose=verbose)
        second_target = first_target + '.tbi'
        if force or not fileExists(second_target):
            make_tabixed(first_target,chrom_field=chrom_field,start_field=start_field,
                         end_field=end_field,verbose=verbose)

def make_compressed(fn,first_target,start_field=None,chrom_field=None,verbose=False):
    cmd = "grep -v track %s | sort --key=%i,%i --key=%i,%in | bgzip -f -c > %s"%(fn,
                                                                                 chrom_field,chrom_field,
                                                                 start_field,start_field,
                                                                 first_target)
    result = runCommand(cmd,verbose=verbose)
    if verbose:
        sys.stderr.write("Result from command: %s is: %s\n"%(cmd,result))
        
def make_tabixed(fn,chrom_field=None,start_field=None,end_field=None,verbose=False):
    cmd =  'tabix -s %i -b %i -e %i %s'%(chrom_field,start_field,end_field,fn)
    runCommand(cmd,verbose=verbose)

def doIt(dir=None,
         force=False,
         verbose=False
         ):
    files = os.listdir(dir)
    for fn in files:
        if verbose:
            sys.stderr.write("Processing: %s\n"%fn)
        processFile(fn=fn,files=files,verbose=verbose)
        
def main(options):
    doIt(dir=options.dir,
         force=options.force,
         verbose=options.verbose
         )

if __name__ == '__main__':
    usage = "%prog [options]\n\nMakes sorted, indexed tabix files for visualization in Integrated Genome Browser and distribution via IGB QuickLoad sites."
    parser = optparse.OptionParser(usage)
    parser.add_option("-d","--dir",
                      help="Directory with files to sort, index. Default is current directory.",
                      dest="dir",
                      default=".")
    parser.add_option("-f","--force",dest="force",action="store_true",help="Force re-writing of files if set.")
    parser.add_option("-v","--verbose",dest="verbose",action="store_true",help="Report commands being executed.")
    (options,args)=parser.parse_args()
    main(options)
