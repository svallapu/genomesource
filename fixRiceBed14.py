#!/usr/bin/env python

ex=\
"""
Write a new BED14 file that inserts locus id into field 13.
Locus id is parsed from transcript id for all transcript ids
that begin with LOC.

example)
  gunzip -c O_sativa_japonica_Oct_2011.bed.gz | %prog > Foo.bed

example)
  %prog test.bed > out.bed

"""

import fileinput,sys,optparse,gzip

sep = '\t'

def main(args=None):
    for line in fileinput.input(args):
        toks=line.split(sep)
        tx_id=toks[3]
        if tx_id.startswith('LOC'):
            locus='.'.join(tx_id.split('.')[:-1]) 
        else:
            locus=tx_id
        toks[12]=locus
        sys.stdout.write(sep.join(toks)) 

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)

