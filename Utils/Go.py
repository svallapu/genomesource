"""Some functions useful for working with Gene Ontology-related files."""


# standard python
import re,sys,os

import Utils.General as gen_utils
import Mapping.Feature as feature

sep1 = '\t'
cg_reg = re.compile(r'CG\d+')
go_reg = re.compile(r'GO:\d+')
# FlyBase gene id
fbgn_reg = re.compile(r'FBgn\d+')

class Annotated(feature.KeyVal,feature.Identifiable):

    def __init__(self,display_id=None):
        feature.Identifiable.__init__(self,display_id=display_id)
        feature.KeyVal.__init__(self)

    def add_annotation(self,key,annotation):
        annots = self.get_val(key)
        if not annots:
            self.set_key_val(key,[annotation])
        else:
            if annotation not in annots:
                annots.append(annotation)

    def get_annotation(self,key):
        return self.get_val(key)

    def add_go(self,annotation):
        self.add_annotation('GO',annotation)

    def get_gos(self):
        self.get_annotation('GO')

def get_go_defs(f='../data/gene_ontology.obo.gz'):
    """
    Function : Get a mapping between GO ids and definitions
    Returns  : dictionary with GO ids as keys and definitions
               as values.
    Args     : f - Gene Ontology 'obo' file
    """
    inf = gen_utils.readfile(f)
    nd = {}
    trees = ['biological_process','molecular_function','cellular_component']
    for tree in trees:
        nd[tree] = {}
    id_reg = re.compile(r'id: (.+)')
    name_reg = re.compile(r'name: (.+)')
    tree_reg = re.compile(r'namespace: (.+)')
    alt_reg = re.compile(r'alt_id: (.+)')
    name = None
    goid = None
    tree = None
    alt_id = None
    while 1:
        line = inf.readline()
        if not line:
            break
        if line.startswith('[Term]'):
            if name:
                nd[tree][goid]=name
            if alt_id:
                nd[tree][alt_id]=name
            name = None
            goid = None
            tree = None
            alt_id = None
            continue
        line = line.rstrip()
        if line.startswith('id:'):
            goid = id_reg.findall(line)[0]
        elif line.startswith('name:'):
            name = name_reg.findall(line)[0]
        elif line.startswith('namespace:'):
            tree = tree_reg.findall(line)[0]
    if name:
        nd[tree][goid]=name
    if alt_id:
        nd[tree][alt_id]=name
    return nd

def get_go_annots_fly(f='../data/gene_association.fb.gz'):
    
    """
    Function : Get a mapping of fruit fly gene ids to
               GO terms
    Returns  : dictionary with gene ids as keys and lists of
               GO ids as values
    Args     : f - data file from geneontology.org
    """
    inf = gen_utils.readfile(f)
    id2go = {}
    num_nocg = 0
    num_nofb = 0
    while 1:
        line = inf.readline()
        if not line:
            break
        if line.startswith('!'):
            continue
        gos = go_reg.findall(line)
        cgs = cg_reg.findall(line)
        fbids = fbgn_reg.findall(line)
        if len(cgs) == 0:
            num_nocg = num_nocg + 1
        if len(fbids) == 0:
            num_nofb = num_nofb + 1
        for cg in cgs:
            if not id2go.has_key(cg):
                id2go[cg] = {}
            for go in gos:
                id2go[cg][go] = cg
        for fb in fbids:
            if not id2go.has_key(fb):
                id2go[fb]={}
            for go in gos:
                id2go[fb][go]=fb
    sys.stderr.write("lines with no CG id: " + repr(num_nocg)+os.linesep)
    sys.stderr.write("lines with no FBgn id: " + repr(num_nofb)+os.linesep)
    return id2go

def get_go_annots_weed(f='../data/gene_association.tair.gz'):
    """
    Function : Get a mapping of Arabidopsis AGI ids to
               GO terms
    Returns  : dictionary with gene ids as keys and lists of
               GO ids as values
    Args     : f - data file from geneontology.org
    """
    inf = gen_utils.readfile(f)
    id2go = {}
    while 1:
        line = inf.readline()
        if not line:
            break
        if line.startswith('!'):
            continue
        else:
            toks = line.rstrip().split(sep1)
            agi = toks[2].split('.')[0]
            go = toks[4]
            if not id2go.has_key(agi):
                id2go[agi]=[]
            if not go in id2go[agi]:
                id2go[agi].append(go)
    return id2go

def get_def(go_defs,goid):
    for tree in go_defs.keys():
        if go_defs[tree].has_key(goid):
            return go_defs[tree][goid]
    return None

def get_tree(go_defs,goid):
    for tree in go_defs.keys():
        if go_defs[tree].has_key(goid):
            return tree
    return None

def get_go_annots_entrez_gene(d,f='../data/gene2go.gz'):
    """
    Function : Get a mapping of Entrez Gene ids to
               GO terms
    Returns  : dictionary with gene ids as keys and lists of
               GO ids as values
               values are lists of tuples, where item 0 is
               the GO id, and item 1 is the evidence code
               for that annotation
    Args     : f - data file from Entrez Gene ftp site
               d - dictionary with Entrez Gene ids as keys
                   (gene2go is huge, so only get annotations for
                   a select group of genes)
    """
    fh = gen_utils.readfile(f)
    d2 = {}
    # get rid of header
    fh.readline()
    while 1:
        line = fh.readline()
        if not line:
            break
        toks = line.rstrip().split(sep1)
        gid = toks[1]
        goid = toks[2]
        ecod =  toks[3]
        if not d.has_key(gid):
            continue
        if not d2.has_key(gid):
            d2[gid]=[[goid,ecod]]
        else:
            prevs = map(lambda x:x[0],d2[gid])
            if goid not in prevs:
                d2[gid].append([goid,ecod])
    return d2
    
    
def gos_for_anal(items,go2def,attr_key='Alt'):
    """
    Function: builds contingency tables for GO term over-represenation
              analysis
    Returns : a 'double' dictionary, first-level keys are the
              GO sub-ontology terms molecular_function, etc
              second-level keys are the GO ids belonging to
              each tree that were used to annotate the given list
              of items (can be genes, proteins, whatever) and
              values are lists representing contingency table counts
              (see below)
    Args    : items - Annotatable objects, where:
                    display_id() retrieves the name of the item
                    (e.g., a gene id)
                    get_val('GO') retrieves a list of GO ids associated
                    with the item
                    get_val(attr_key) retrieves whether or not the item
                    is in the other category, or not (e.g., differentially-
                    expressed, alternatively-spliced, etc.) This corresponds
                    to the rows in the contingency table below.
              go2def - output from get_go_def        
              attr_key - the key that retrieves the "row" category
                   
    The contingency tables will allow a comparison of a gene's
    membership in a GO category with whether or not it is in some
    other category X, e.g., part of a list of differentially expressed
    genes, alternatively spliced, etc. (The rows in the contingency
    table below.)

    Counts will be made for each sub-ontology separately -
    molecular_function, biological_process, cellular_component.
    
    e.g.,

          inGO    notinGO
    inX     a       b       tin
    outX    c       d       tout
            tg      tng     TOTAL

           TOTAL: total number of genes in sub-ontology
           a: items in X that are also in the GO category
           b: items in X that are *not* in the GO category
           c: items not in X that are in the GO category
           d: items not in X that are *not* in the GO category

    Note: TOTAL = a + b + c + d; tin = a + b; tng = b + d; etc.
    
    Structure of the contingency table lists:
          [a,b,c,d]

    Where a,b,d,d are counts (integers)
    """
    # get all the annotated items contained in each
    # sub-ontology
    tree2annotated = get_tree2annotated(items,go2def)

    # create a mapping between GO ids and lists of items
    # that are annotated with these GO ids
    go2annotated = get_go2annotated(items)
    
    # the contingency tables, with sizes, keyed by GO tree,
    # and then GO ids for terms belonging to the given tree
    ctables = {}
    
    for tree in tree2annotated.keys():
        ctables[tree] = {}
        # all the items that have been annotated by
        # at least one term in this sub-ontology tree
        annotated = tree2annotated[tree].values()
        # all the goids used to annotate at least one item
        # which are also in this sub-ontology tree
        goids = filter(lambda x:go2def[tree].has_key(x),
                       go2annotated.keys())
        for goid in goids:
            # all the items attached to this GO id
            item_list = go2annotated[goid]
            in_go_dict = {}
            ctable =[0,0,0,0] # a,b,c,d
            for item in item_list:
                in_go_dict[item.display_id()]=1
                in_X = i.get_val(attr_key)
                if in_X:
                    ctable[0]=ctable[0]+1
                else:
                    ctable[3]=ctable[3]+1
            # all the items *not* attached to this GO id
            # (whatever didn't get added to in_go_dict)
            for item in item_list:
                if in_go_dict.has_key(g.display_id()):
                    continue
                in_X = i.get_val(attr_key)
                if in_X:
                    ctable[1]=ctable[1]+1
                else:
                    ctable[3]=ctable[3]+1
            sys.stderr.write("Done: " + tree + ":" + goid + os.linesep)
    return ctables

def get_tree2annotated(items,go2def,attr=None):
    """
    Function: sort the annotated items into a 'double' dictionary
              where first level keys are the sub-ontologies
              (molecular_function, cellular_component, etc.) and the
              second level keys are the items's display_ids and the
              values are the Annotated objects 
    Returns : a double-dictionary
    Args    : items - a list of items
                      each should have a unique display_id
              go2def - output from get_go_defs, mapping of GO ids
                       to GO definitions
              attr - [optional] only consider items with this key val
                     attribute set to 1

Note that some terms used to annotate the items  may be
obsolete and therefore will not appear in go2def. We will simply
exclude these from any analysis.
"""
    d = {}
    for tree in go2def.keys():
        d[tree] = {}
        for item in items:
            if attr:
                if not item.get_val(attr)==1:
                    continue
            goids = item.get_val('GO')
            goids = filter(lambda x:go2def[tree].has_key(x),goids)
            if len(goids)>0:
                d[tree][item.display_id()]=item
    return d

def count_annotations(items,go_defs,fn=None,attr=None):
    """
    Function: Count the number items that have various annotations
    Returns : 
    Args    : items - Annotated objects, with GO ids added
              go_defs - output from get_go_defs
              fn - [optional] write results to this file, or stderr
                   if not given
              attr - [optional] only consider items with this attr
                     set to 1
    """
    go2annotated = get_go2annotated(items,attr=attr)
    if not fn:
        fh = sys.stderr
    else:
        fh = open(fn,'w')
    if attr:
        heads = ['tree','GO','defn',attr]
    else:
        heads = ['tree','GO','defn','items']
    fh.write(sep1.join(heads)+os.linesep)
    for tree in go_defs.keys():
        for goid in go_defs[tree].keys():
            defn = go_defs[tree][goid]
            if not go2annotated.has_key(goid):
                continue
            annotateds = go2annotated[goid] 
            if attr:
                attrs = filter(lambda x:x.get_val(attr)==1,annotateds)
            else:
                attrs = annotated
            if len(attrs) == 0:
                continue
            vals = [tree,goid,defn,repr(len(attrs))]
            fh.write(sep1.join(vals)+os.linesep)
    if fn:
        fh.close()

            
def get_go2annotated(items,attr=None):
    """
    Function: create a mapping between GO ids and the items
              they annotate
    Returns : a dictionary of GO ids as keys and lists of the
              items they annotate as values
    Args    : items - Annotated objects
              attr - [optional] only consider items with this key val
                     attribute set to 1
    """
    d = {}
    for item in items:
        if attr:
            if not item.get_val(attr)==1:
                continue
        go_ids = item.get_val('GO')
        for go_id in go_ids:
            if not d.has_key(go_id):
                d[go_id]=[]
            d[go_id].append(item)
    return d

"""
To use the over-representation analysis functions:

(1) Get your list of items that were considered for membership in
    category X

    e.g., all the genes in a genome

(2) For each of these, call set_key_val with some attribute key
    (e.g., 'diff_expressed')
    Set that attribute to be 0 if the item is not in the category,
    1 if it is.

(3) Next, read GO annotations from some gene_association file, e.g.,
    gene_association.tair.gz, or whatever

(4) Loop over the Annotated items and add GO annotations.

(5) Next, build the contingency tables for each GO term using
    gos_for_anal.

(6) Write out the contingency tables. 

(7) Analyze using R's fisher.test function.


Note: There is no code here to walk up the tree and test parental terms
for over-representation. 
"""

def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass

"""
From

ftp://ftp.ncbi.nlm.nih.gov/gene/README

===========================================================================
gene2go
---------------------------------------------------------------------------
           This file reports the GO terms that have been associated
            with Genes in Entrez Gene.
           It is generated by processing the gene_association files
            on the GO ftp site: 
            http://www.geneontology.org/GO.current.annotations.shtml
           and comparing the DB_Object_ID to annotation in Gene,
           as also reported in gene_info.gz

           Multiple gene_associations file may be used for any genome.
           If so, duplicate information is not reported; but unique
           contributions of GO terms, evidence codes, and citations are.

           In the directory 
             ftp://ftp.geneontology.org/pub/go/gene-associations/

           the files that are used are:
           Arabidopsis thaliana
                                 gene_association.tair.gz
                                 gene_association.tigr_Athaliana.gz

           Bacillus anthracis Ames
                                 gene_association.tigr_Banthracis.gz

           Caenorhabditis elegans
                                 gene_association.wb.gz

           Coxiella burnetii RSA 493
                                 gene_association.tigr_Cburnetii.gz

           Danio rerio             
                                 gene_association.zfin.gz

           Drosophila melanogaster
                                 gene_association.fb.gz

           Geobacter sulfurreducens PCA
                                 gene_association.tigr_Gsulfurreducens.gz

           Homo sapiens
                                 gene_association.goa_human

           Listeria monocytogenes 4b F2365
                                 gene_association.tigr_Lmonocytogenes.gz

           Methylococcus capsulatus Bath
                                 gene_association.tigr_Mcapsulatus.gz

           Mus musculus
                                 gene_association.mgi

           Oryza sativa          
                                 gene_association.gramene_oryza.gz

           Plasmodium falciparum
                                 gene_association.GeneDB_Pfalciparum.gz

           Pseudomonas syringae DC3000
                                 gene_association.tigr_Psyringae.gz

           Rattus norvegicus
                                 gene_association.rgd

           Shewanella oneidensis MR-1
                                 gene_association.tigr_Soneidensis.gz

           Trypanosoma brucei chr 2
                                 gene_association.tigr_Tbrucei_chr2.gz

           Saccharomyces cerevisiae         
                                 gene_association.sgd.gz

           Schizosaccharomyces pombe
                                 gene_association.GeneDB_Spombe.gz



           This file can be considered as the logical equivalent of

                    ftp://ftp.ncbi.nih.gov/refseq/LocusLink/loc2go



           MODIFIED: May 9, 2006 to include the category of the GO term.


           One line per GeneID/GO term/representative GO evidence code.
           Column header line is the first line in the file.
---------------------------------------------------------------------------

tax_id:
           the unique identifier provided by NCBI Taxonomy
           for the species or strain/isolate

GeneID:
           the unique identifier for a gene
           --note:  for genomes previously available from LocusLink,
                    the identifiers are equivalent

GO ID:
           the GO ID, formatted as GO:0000000

Evidence:
           the evidence code in the gene_association file

Qualifier: 
           a qualifier for the relationship between the gene
           and the GO term

GO term:
           the term indicated by the GO ID

PubMed:
           pipe-delimited set of PubMed uids reported as evidence
           for the association

Category:
           the GO category (Function, Process, or Component)

"""
