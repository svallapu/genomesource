import re,string,sys

import Mapping.Feature as feature
import Utils.General as utils

"""
Functions and objects for dealing with probe set annotations and
alignment files downloaded from Affymetrix.  Note that probe set ids
are in a different format in these two different types of files.
"""

array_codes = ['MG-U74AV2']

class ProbeSet(feature.Identifiable):

    def __init__(self,display_id=None):
        feature.__init__(display_id=display_id)
        self._target_ids = []
        
    def target_ids(self):
        """
        Function: Get ids of probeset targets.
        Returns : A list of targets (may be more than one)
        Args    : 
        """
        return self._target_ids

    def add_target_id(self,id):
        """
        Function: Add a new target id, typically mRNA accessions.
        Returns : 
        Args    : id - the id of a target sequence
        """
        if not ids in self._target_ids:
            self._target_ids.append(id)
    
    def add_gene_id(self,id):
        """
        Function: Add a new target gene id.
        Returns : 
        Args    : id - a gene id, e.g., a gene symbol or Entrez Gene
                      id 
        """
        if not id in self._gene_ids:
            self._gene_ids.append(id)

    def add_target(self,feat):
        """
        Function: Add the given feature as a potential target
                  of this Probeset
        Returns : 
        Args    : feat - a SeqFeature of some type
        """
        if feat not in self._targets:
            self._targets.append(feat)



def link_array_code(array):
    """
    Function: Get the name NetAffx uses to build links to
              the given array
    Returns : 
    Args    : array - array model, e.g. ATH1

    need to implement synonyms lookup!
    """
    if array == 'ATH1':
        return 'ATH1-121501'
    if array in ['Mouse430_2','Mu430_2','MOE430']:
        return 'MOUSE430_2'
    if array in ['u74v2A']:
        return 'MG-U74AV2'
    else:
        return None

def ps_fullrecord_url(ps,array):
    """Function: build URL to Affymetrix probe set page
    Returns : URL
    Args    : ps - probeset or probeset list
              array - array model, e.g., ATH1 or synonyms
    """
    url = None
    code = link_array_code(array)
    if code:
        url = 'https://www.affymetrix.com/analysis/netaffx/fullrecord.affx?pk='+code + ':' + ps
    return url

def ps_url(ps,array):
    """
    Function: build URL to Affymetrix probe set results page
    Returns : URL
    Args    : ps - probeset or probeset list
              array - array model, e.g., ATH1 or synonyms

    Note: this still will require an additional click
    """
    url = 'https://www.affymetrix.com/LinkServlet?array='+\
          link_array_code(array)+'&probeset='
    if utils.is_list(ps):
        url = url + string.join(ps,',')
    else:
        url = url + ps
    return url

def ps_link(ps,array,link_label):
    """
    Function: create a hyperlink to the url that was created
    Returns : hyperlinked URL
    Args    : ps - probeset or probeset list, array - array model e.g. ATH1 or synonyms, link_labels
    """
    url = ps_url(ps,array)
    return '<a href="'+url+'">'+link_label+'</a>'

def get_affy_id_regex():
    """
    Function: create a regular expression that matches an Affymetrix id
    Returns : a compiled regular expression
    Args    : nothing
    """
    return re.compile(r'\d\d\d+_[{\w_at}\{at}]',re.M)
    
def split_affy_annots_line(line):
    """
    Function: get all the fields from a csv-format annotations line
    Returns : a list of tokens
    Args    : a string

    Note that fields can contain ',' characters.  Thus splitting
    on the comma separator is not a good idea.
    """
    toks = line.split('","')
    toks[0]=toks[0][1:]
    toks[-1]=toks[-1][0:-2]
    return toks

def read_affy_annots(f,pss=None):
    """
    Function: read annotations file 
    Returns : a dictionary, keys are the probe set ids
    Args    : f - the name of the annotations file (can be compressed)
              pss - an optional list of probe sets to include in the
                    output
                    
    The returned dictionary contains sub-dictionaries, keyed by
    the probeset ids.

    The sub-dictionary keys are whatever was given in the first
    line as column headings.

    Note that Affymetrix uses the string '---' to indicate NA (not
    available) values.
    """
    inf = utils.readfile(f)
    line = inf.readline()
    fields = split_affy_annots_line(line)
    to_return = {}
    while 1:
        line = inf.readline()
        if not line:
            break
        vals = split_affy_annots_line(line)
        key = vals[0]
        if pss and key not in pss:
            continue
        f_iter = 0
        vals_dict = {}
        for field in fields:
            val = vals[f_iter]
            vals_dict[field] = val
            f_iter = f_iter + 1
        to_return[key] = vals_dict
    return to_return

def clear_controls(d):
    """
    Function: removes control probe sets from the given dct
    Returns : a new dictionary purged of control probe sets
    Args    : a dictionary in which the keys are probe set ids

    Results, annotation, and alignment files typically contain control
    probe set ids, which usually need to be removed prior to analysis.
    """
    nd = {}
    for ps in d.keys():
        if is_control(ps):
            continue
        else:
            nd[ps]=d[ps]
    return nd

def is_control(ps):
    """
    Function: find out if the given probe set id is a control probe set
    Returns : True if it's a control
              False if it's not a control
    Args    : a probe set id
    """
    if re.search(r'AFFX',ps,re.I):
        return 1
    else:
        return 0


def count_genes(d):
    """
    Function: returns a count of probe sets and Entrez Gene ids
    Returns : a tuple, item 0 is a dictionary mapping Entrez Gene ids (numeric)
              onto lists of probe sets ids, item 1 is a dictionary mapping concatenated
              Entrez Gene ids (strings) onto probe set ids
    Args    : d -dictionary returned from read_affy_annots
    """
    bogus_ids = {}
    okay_ids = {}
    for pid in d.keys():
        p = d[pid]
        gene_id = p['Entrez Gene']
        try:
            string.atoi(gene_id)
        except ValueError:
            if bogus_ids.has_key(gene_id):
                bogus_ids[gene_id].append(pid)
            else:
                bogus_ids[gene_id] = [pid]
            continue
        if okay_ids.has_key(gene_id):
            okay_ids[gene_id].append(pid)
        else:
            okay_ids[gene_id]=[pid]
    return (okay_ids,bogus_ids)

def count_probesets(d):
    """
    Function: tally probe sets associated with Entrez Gene ids
    Returns : a dictionary where the number of redundant probe sets per
              Gene id is the key and number of Entrez Gene ids are the values
    Args    : a dictionary returned from count_genes
    """
    probesets = {}
    gcount = {}
    for gene_id in d.keys():
        pss = d[gene_id]
        numps = len(pss)
        if gcount.has_key(numps):
            gcount[numps]=gcount[numps]+1
        else:
            gcount[numps] = 1
    return gcount

def count_all_unique_vals(dct,na='---'):
    """
    Function: counts the number of unique and missing values in
              a dictionary returned by read_affy_annots
    Returns : nothing
    Args    : a dictionary returned by read_affy_annots

    """
    keys = get_fields(dct).keys()
    for key in keys:
        kt = count_unique_vals(dct,key,na)
    
def count_unique_vals(dct,field,na='---'):
    """
    Function: 
    Returns : A dictionary with keys equal to the values inder the
              given field heading read from an Affymetrix annotations file.
              Values are a list of probesets that had that value.
    Args    : dct - dictionary returned by read_affy_annots
              field - subdictionary key, a column heading from the annotatiosn
                      file
              na = optional, the missing value field ( --- ) is the default
    """
    keys = dct.keys()
    keeping_track = {}
    for key in keys:
        subdct = dct[key]
        if subdct.has_key(field):
            val = subdct[field]
            if keeping_track.has_key(val):
                keeping_track[val].append(key)
            else:
                keeping_track[val] = [key]
    n_unique_vals = len(keeping_track.keys())
    num_nas = 0
    if keeping_track.has_key(na):
        num_nas = len(keeping_track[na])
    print field + ',' + repr(n_unique_vals) + ',' +\
          repr(num_nas)
    return keeping_track

def get_transcript_ids(dct):
    """
    Function: retrieve Genbank identifiers associated with the probeset
              ids saved in the given dct
    Returns : a dictionary with keys equal to Genbank identifiers and
              values are a lists of probesets associated with that identifier
    Args    : dct - a dictionary as returned by read_affy_annots

    """
    gb2probeset = {}
    vals = count_unique_vals(dct,'Target Description')
    gbreg = re.compile(r'gb:(\S+)')
    for val in vals.keys():
        probesets = vals[val]
        matches = gbreg.findall(val)
        if len(matches) == 0:
            print "no genbank for : " + string.join(probesets)
        else:
            for match in matches:
                if not gb2probeset.has_key(match):
                    gb2probeset[match] = probesets
                else:
                    for probeset in probesets:
                        if probeset not in gb2probeset[match]:
                            gb2probeset[match].append(probeset)
    return gb2probeset

def purge_versions(dct):
    """
    Function: returns a new dictionary in which the keys are accessions
              with version suffixes removed.
    Returns : a dictionary with accessions as keys and values the same
              as the original dictionary
    Args    : dct - a dictionary where keys are accesssions, with or
              without the version suffixes

    """
    ndct = {}
    regex = re.compile(r'\.\d+')
    for key in dct.keys():
        newkey = regex.sub('',key)
        if ndct.has_key(newkey):
            raise Exception("Accession present twice: " + newkey)
        else:
            ndct[newkey]=dct[key]
    return ndct


def get_ests(dct,fs=['/data/mm/mm5/est.mm5.gtf.gz']):
    """
    Function: 
    Returns :
    Args    : dct- a dictionary
              fs - list of files in gtf format from UCSC
                   Table Browser
    """

    (ndct,gtf_dct) = filter_gtf(dct,fs)
    write_lines(gtf_dct,'filtered_ests.mm5.gtf')
    return (ndct,gtf_dct)

def write_lines(dct,fn):
    """
    Function: writes the key in the dictionary to the file
    Returns : nothing
    Args    : dct - a dictionary with keys 
              Fn - name of the file to be written

    """

    outfh = open(fn,'w')
    for key in dct.keys():
        lines = dct[key]
        for line in lines:
            outfh.write(line)
    outfh.close()


def filter_gtf(dct,fs):
    """
    Function: collects GTF lines from the given files where the
              transcript ids match the keys in the given dictionary
    Returns : a dictionary where keys are from dct and values are a
              list of GTF lines from the given files
    Args    : dct - a dictionary with keys corresponding to 'transcript_id'
              values in the extra feature field of a GTF file
              fs - a list of files

    Note that if the same key is present in different files, only the lines
    from the first file will be collected.

    """

    gtf_dict = {}
    ndct = purge_versions(dct)
    save_dct = purge_versions(dct)
    tx_reg = re.compile(r'transcript_id \"(.*)\"')
    for f in fs:
        inf = utils.readfile(f)
        while 1:
            line = inf.readline()
            if not line:
                break
            matches = tx_reg.findall(line)
            if len(matches) > 0:
                tx_id = matches[0]
                if ndct.has_key(tx_id):
                    if not gtf_dict.has_key(tx_id):
                        gtf_dict[tx_id]= []
                    gtf_dict[tx_id].append(line)
        inf.close()
        for key in ndct.keys():
            if gtf_dict.has_key(key):
                del(ndct[key])
        
        
    return (save_dct,gtf_dict)

def read_mappings(f):
    """
    Function: 
    Returns : a dictionary where track names are keys to subdictionaries
              containing mappings of query names
              note that a single query name may map more than once
    Args    : f- name of the 'link' file from Affymetrix
                 containing probeset-to-genome mappings

    Eliminates control probe sets, probe sets with 'affx' in the id
    """
    inf = utils.readfile(f)
    track_reg = re.compile(r'track name="(.*?)"')
    reg = re.compile(r'Probe Sets')
    curr_track = None
    tracks = {}
    while 1:
        line = inf.readline()
        if not line:
            break
        # ignore comments
        if line.startswith('#'):
            continue
        
        if line.startswith('track'):
            curr_track = track_reg.search(line).groups()[0]
            tracks[curr_track]={}
            continue
        qname = line.rstrip().split('\t')[9]
        if qname.split(':')[1].lower().startswith('affx'):
            continue
        if not tracks[curr_track].has_key(qname):
            tracks[curr_track][qname]=[line.rstrip()]
        else:
            tracks[curr_track][qname].append(line.rstrip())
    inf.close()
    return tracks

def assess_mappings(d):
    """
    Function :
    Returns  :
    Args     :
    """
    
    track_reg = re.compile('consensus')
    con_key = None
    for key in d.keys():
        if track_reg.search(key):
            con_key = key
            break
    subd = d[con_key]
    nps = len(subd.keys())
    sys.stderr.write(repr(nps) + ' probe sets\n')
    nmaps = {}
    for ps in subd.keys():
        nmap = len(subd[ps])
        if nmaps.has_key(nmap):
            nmaps[nmap]=nmaps[nmap].append(subd[ps])
        else:
            nmaps[nmap]=[subd[ps]]
    return nmaps

def get_okay_probesets():
    """Function: 
    Returns : 
    Args    : 
    """

    d = {}
    read_mappings(d)
    nd = {}
    for key in d.keys():
        if len(d[key]) > 1:
            continue
        else:
            nkey = key.split(':')[1].lower()
            nd[nkey] = d[key]
    return nd

def test(f="Mouse430_2_annot.csv"):
    """
    Function: creates a dictionary where keys are the probe set ids
    Returns : 
    Args    : f - name of annotation file that can be compressed
    """
    d = read_affy_annots(f)
    d = clear_controls(d)
    (okay,bogus) = count_genes(d)
    mappings = read_mappings({})
