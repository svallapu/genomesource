"""Set up QuickLoad files for Dicty (slide mold). Functions are meant to be run interactively."""

from Bio import SeqIO
import re,sys,os
import Mapping.Feature as f
import Mapping.Parser.Bed as b

heads = """>DDB0169550 |Chromosomal Sequence| Chromosome: M position 1 to 55564
>DDB0215018 |Chromosomal Sequence| Chromosome: 2F position 1 to 161967
>DDB0215151 |Chromosomal Sequence| Chromosome: 3F position 1 to 16660
>DDB0220052 |Chromosomal Sequence| Chromosome: BF position 1 to 75732
>DDB0232428 |Chromosomal Sequence| Chromosome: 1 position 1 to 4923596
>DDB0232429 |Chromosomal Sequence| Chromosome: 2 position 1 to 8484197
>DDB0232430 |Chromosomal Sequence| Chromosome: 3 position 1 to 6357299
>DDB0232431 |Chromosomal Sequence| Chromosome: 4 position 1 to 5450249
>DDB0232432 |Chromosomal Sequence| Chromosome: 5 position 1 to 5125352
>DDB0232433 |Chromosomal Sequence| Chromosome: 6 position 1 to 3602379
>DDB0237465 |Chromosomal Sequence| Chromosome: R position 1 to 85150"""

# original GFF files downloaded from DictyBase, July 6, 2010
gff_fns = ['chromosome_1.gff', 'chromosome_2.gff', 'chromosome_2F.gff', 'chromosome_3.gff', 'chromosome_3F.gff', 'chromosome_4.gff', 'chromosome_5.gff', 'chromosome_6.gff', 'chromosome_BF.gff', 'chromosome_M.gff', 'chromosome_R.gff']


reg = re.compile(r'Chromosome: (\S+) position')
preg = re.compile(r'Parent=(DDB_?\S\d+)')
idreg = re.compile(r'ID=(DDB_?\S\d+)')
# downloaded from http://www.dictybase.org/Downloads/
def readGenome(fn="dicty_chromosomal"):
    fh = open(fn)
    iter = SeqIO.parse(fh,'fasta')
    for rec in SeqIO.parse(fh,'fasta'):
        header = rec.description
        chrom_name = reg.findall(header)[0]
        chrom_len = len(rec.seq)
        sys.stdout.write("chr%s\t%i\n"%(chrom_name,chrom_len))
        chrom_name = 'chr'+chrom_name
        old_id = rec.id
        rec.id=chrom_name
        rec.description='D_discoideum_Ax4_May_2005 %i bp %s'%(chrom_len,old_id)
        seqfn = 'dna/%s.fa'%chrom_name
        seqfh = open(seqfn,'w')
        SeqIO.write([rec],seqfh,'fasta')
        seqfh.close()
    fh.close()

# synonyms of sequences
def getNameMappings():
    d = {}
    lines = heads.split('\n')
    for line in lines:
        chrom_name = 'chr'+reg.findall(line)[0]
        did = line.split(' ')[0][1:]
        d[chrom_name]=did
        d[did]=chrom_name
    return d

# get rid of the sequence at the end of the file
def makeNoSeqGFF():
    for fn in gff_fns:
        fh = open(fn,'r')
        newfn = 'chr'+fn.split('_')[1]+'3'
        outfh = open(newfn,'w')
        while 1:
            line = fh.readline()
            if not line:
                outfh.close()
                fh.close()
                break
            if line.startswith('#'):
                outfh.write(line)
            if not len(line.split('\t'))==9:
                continue
            else:
                outfh.write(line)

# make beds
def makeBeds():
    # assumes we're in the same directory with the gff files, each one
    # named chr1.gff3, etc
    fns = os.listdir('.')
    prods = {}
    for fn in fns:
        if fn.endswith('.gff3') and fn.startswith('chr') and not fn.startswith('chromosome'):
            d = makeGeneModelsDict(fn=fn)
            feats = procGeneModels(rnas_d=d)
            for feat in feats:
                prod = feat.getProducer()
                if prod.endswith('Curator'):
                    prod = 'dictyBase Curator'
                #if feat.getStrand()==-1:
                #    feat.setKeyVal('rgb','255,0,0')
                #else:
                #    feat.setKeyVal('rgb','0,0,255')
                if not prods.has_key(prod):
                    prods[prod]={}
                if prods[prod].has_key(feat.getDisplayId()):
                    sys.stderr.write("Warning: duplicate feat with display id: %s\n"%
                                     feat.getDisplayId())
                    raise ValueError("Can't proceed!")
                else:
                    prods[prod][feat.getDisplayId()]=feat
    for prod in prods.keys():
        feats = prods[prod]
        if prod == '.':
            prod = "DictyDB"
        tname=prod
        toks = prod.split(' ')
        fn = '_'.join(toks)+'.bed'
        # b.feats2bed(feats,fname=fn,tname=tname,rgb=True)
        b.feats2bed(feats,fname=fn,tname=tname)
    sys.stderr.write("Done!\n")
                    
                    
            
            
# read gene and rna annotations and make gene models
def makeGeneModelsDict(fn='chr1.gff3'):
    gene_reg = re.compile(r'ID=(DDB_G\d+)')
    chrom = fn.split('.')[0]
    lookup_d = getNameMappings()
    dd_chrom = lookup_d[chrom]
    fh = open(fn)
    genes_d = {}
    mrnas_d = {}
    mrna2gene = {}
    ESTs=[]
    gaps=[]
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        if not line.startswith(dd_chrom):
            continue
        toks = line.rstrip().split('\t')
        if toks[2]=='gene':
            gene_id = gene_reg.findall(toks[-1])[0]
            genes_d[gene_id]=[line]
    # get the mRNAs
    fh = open(fn)
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        if toks[2]=='gap':
            gaps.append(line)
            continue
        if toks[2]in('mRNA','tRNA','class_I_RNA','snRNA','SRP_RNA',
                     'class_II_RNA','C_D_box_snoRNA','RNase_P_RNA',
                     'RNase_MRP_RNA','rRNA','ncRNA','H_ACA_box_snoRNA'
                     ):
            try:
                parent = getParent(toks[-1])
                mrna_id = getId(toks[-1])
            except IndexError:
                sys.stderr.write("Choked on: %s\n"%line)
                break
            genes_d[parent].append({mrna_id:line})
            mrnas_d[mrna_id]=[line]
            if mrna2gene.has_key(mrna_id):
                sys.stderr.write("Warning: multiple genes for %s\n"%mrna_id)
                break
            mrna2gene[mrna_id]=gene_id
            # this happens because DictyDB associates multiple mRNAs to the
            # same gene when they curate a gene mode. Thus, they may be
            # redundant. You can tell the difference by looking at the producer
            # fields. So...for the purposes of making bed files, we'll make
            # separate beds for the different types of gene models, which
            # each have their own id but refer to the same gene id and thus
            # have the same parent in the Dicty DB GFF
            #if len(d[parent])>2:
            #    sys.stderr.write("Warning: got multiple mRNAs for %s\n"%parent)
            continue
        if toks[2]=='exon' or toks[2]=='CDS':
            try:
                parent = getParent(toks[-1])[0]
            except IndexError:
                sys.stderr.write("Choked on: %s\n"%line)
                break
            mrnas_d[mrna_id].append(line)
            continue
        if toks[2]=='EST_match':
            ESTs.append(line)
            continue
        if toks[2] in ("chromosome","contig","gene"):
            continue
        sys.stderr.write("Warning: didn't know what to do with line:\n%s"%line)
        break
    sys.stderr.write("Done!\n")
    return mrnas_d


# process gene models and turn them into CompoundDNASeqFeature objects
# rnas_d - keys are rna ids and values are relevant GFF lines
# and write out bed lines
def procGeneModels(rnas_d=None):
    feats=[]
    illegal_types={}
    lookup_d = getNameMappings()
    for rna_id in rnas_d.keys():
        lines = rnas_d[rna_id]
        firstline = lines[0]
        toks=firstline.split('\t')
        gene_id=getParent(toks[-1])
        original_seqname=toks[0]
        seqname = lookup_d[original_seqname]
        start = int(toks[3])-1
        end = int(toks[4])
        length=end-start
        original_strand=toks[6]
        producer=toks[1]
        if original_strand=='+':
            strand = 1
        elif original_strand == '-':
            strand = -1
        else:
            sys.stderr.write("Warning: bogus strand for %s in line %s\n"%
                             (rna_id,line))
            return
        try:
            feat = f.CompoundDNASeqFeature(display_id=rna_id,
                                           start=start,
                                           length=length,
                                           strand=strand,
                                           feat_type=toks[2],
                                           producer=toks[1],
                                           seqname=seqname)
            for line in lines[1:]:
                toks = line.split('\t')
                if not toks[0] == original_seqname:
                    sys.stderr.write("Warning: child feature seqname and parent seqname do not match: %s"%line)
                    return
                if not toks[6]==original_strand:
                    sys.stderr.write("Warning: child feature strand and parent strand do not match: %s"%line)
                    return
                if not toks[1]==producer:
                    sys.stderr.write("Warning: child feature producer and parent producer do not match: %s"%line)
                    return
                start = int(toks[3])-1
                end = int(toks[4])
                length=end-start
                child_feat = f.DNASeqFeature(display_id=rna_id,
                                             start=start,
                                             length=length,
                                             strand=strand,
                                             seqname=seqname,
                                             producer=producer,
                                             feat_type=toks[2])
                feat.addFeat(child_feat)
            feats.append(feat)
        except ValueError:
            if not illegal_types.has_key(toks[2]):
                sys.stderr.write("Illegal Type: %s\n"%toks[2])
                
                illegal_types[toks[2]]=1
    return feats


# EST alignments make no sense, e.g.,
# 'DDB0232428\t.\tEST_match\t4903906\t4904517\t.\t+\t.\tID=DDB0297851;Target=DDB0297851 1 500;Gap=M115 I113 M383 \n'
# note that in this case, the M (matches?) do not add up to 500, which appears to
# be the length of the EST aligned onto the genome
# also, there is no Genbank id and so we have no idea what EST was aligned
# to create this EST_match feature
def procESTs(ests=None):
    for line in ests:
        pass
    

# ftp://ftp.broadinstitute.org/distribution/igv/ISMB2010/
def getParent(s):
    return preg.findall(s)[0]

def getId(s):
    return idreg.findall(s)[0]
