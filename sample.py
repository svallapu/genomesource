#!/usr/bin/env python

import sys, argparse

def main(integers=None,func=None):
    sys.stdout.write("Success!\n")
    if integers:
        print func(integers)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test-run some python code.')
    parser.add_argument('integers', metavar='N', type=int, nargs='*',
                        help='Your favorite numbers.')
    parser.add_argument('--sum', dest='accumulate', action='store_const',
                        const=sum, default=max,
                        help='sum the integers (default: find the max)')
    args = parser.parse_args()
    main(integers=args.integers,func=args.accumulate)
    
    
